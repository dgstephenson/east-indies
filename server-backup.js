const express = require('express')
const http = require('http')
const socketIo = require('socket.io')
const path = require('path')

const app = express()
const server = http.Server(app)
const io = socketIo(server)

app.use(express.static(path.join(__dirname,'public')))

const updates = []
const numplayers = 4
const seed = Math.random().toString()

app.get('/', (request,response) => 
    response.sendFile(path.join(__dirname,'public','client.html'))
)

io.on('connection', socket => {
  console.log("socket.id =",socket.id)
  socket.emit("setup",{
    seed,
    numplayers,
    updates: updates.filter(update=>{return true}),
  });
  socket.on('updateServer', function(msg){
    var socketIds = Object.keys(io.sockets.connected);
    msg.updates.map( update => {
      updates[update.id] = update;
      socketIds.map( id => {
        var othersocket = io.sockets.connected[id];
        if(id!=socket.id) {
          msg.seed = seed
          othersocket.emit("updateClient",msg);
        }
      });
    });
  });
});

server.listen(3000, function(){
  var port = server.address().port
  console.log('listening on *:%s', port);
})
