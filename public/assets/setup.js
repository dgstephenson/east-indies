var numPlayers = 4
var unitsPerPlayer = Math.ceil(40/numPlayers)
var numBottomRowPlayers = Math.round(numPlayers/2)
var numTopRowPlayers = numPlayers-numBottomRowPlayers
var topRowOrigins = []
var bottomRowOrigins = []
for(i=0; i<numTopRowPlayers; i++) {
  var alpha = (i+1)/(numTopRowPlayers+1)
  var x = -2500*alpha+2500*(1-alpha)
  topRowOrigins.push([x,-1450])
}
for(i=0; i<numBottomRowPlayers; i++) {
  var alpha = (i+1)/(numBottomRowPlayers+1)
  var x = -2500*alpha+2500*(1-alpha)
  bottomRowOrigins.push([x,1450])
}
origins = topRowOrigins.concat(bottomRowOrigins)

shuffle = v => {
  for(
    var j, x, i = v.length; i; j = parseInt(Math.random() * i),
    x = v[--i],
    v[i] = v[j],
    v[j] = x
  );
  return v;
};

var playerColors = [
  'blue','brown','green','grey','orange',
  'pink','purple','red','yellow'
]

var marketOrder = [
  "army","army","army","fleet","fleet","fleet",
  "commander","commander","merchant","merchant"
];

const describeBitRow = function(componentList,file,x,y,n,length) {
  for(i=0; i<n; i++) {
    var alpha = 0
    if(n>1) alpha = i/(n-1)
    var myX = x*(1-alpha) + (x+length)*alpha
    description = describeComponent(file,myX,y,'bit',1)
    componentList.push(description)
  }
}

const describePortfolioBitRow = function(componentList,file,x,y,n,length) {
  fileList = []
  if(Array.isArray(file)) {
    fileList = file
  } else {
    for(i=0; i<numPlayers; i++) fileList[i] = file
  }
  topRowOrigins.map( (origin,i) => {
    describeBitRow(componentList,fileList[i],origin[0]+x,origin[1]+y,n,length)
  })
  bottomRowOrigins.map( (origin,i) => {
    describeBitRow(componentList,fileList[i+numTopRowPlayers],origin[0]+x,origin[1]-y,n,length)
  })
}

const setup = message => {
  marketOrder = shuffle(marketOrder).slice(0,5)
  playerColors = shuffle(playerColors).slice(0,numPlayers)
  playerUnits = playerColors.map(color=>"unit/"+color)
  componentList = [
    describeComponent('board/map',-725,0,'board',1),
    describeComponent('board/market',1100,400,'board',1),
    describeComponent('board/bank',1100,-200,'board',1)
  ]
  topRowOrigins.map(origin=>{
    var portfolioDescription = describeComponent('board/portfolio',origin[0],origin[1],'board',1,180)
    var prisonDescription = describeComponent('board/prison',origin[0],origin[1]-600,'board',1,180)
    componentList.push(portfolioDescription,prisonDescription)
  })
  bottomRowOrigins.map(origin=>{
    var portfolioDescription = describeComponent('board/portfolio',origin[0],origin[1],'board',1)
    var prisonDescription = describeComponent('board/portfolio',origin[0],origin[1],'board',1,180)
    var prisonDescription = describeComponent('board/prison',origin[0],origin[1]+600,'board',1,180)
    componentList.push(portfolioDescription,prisonDescription)
  })
  describePortfolioBitRow(componentList,"gold/5",-400,30,10,400)
  describePortfolioBitRow(componentList,"gold/1",-400,250,10,400)
  describePortfolioBitRow(componentList,playerUnits,-400,-250,unitsPerPlayer,800)
  describePortfolioBitRow(componentList,"contract/merchant",250,30,1,100)
  describePortfolioBitRow(componentList,"contract/commander",400,30,1,100)
  describePortfolioBitRow(componentList,"contract/army",250,250,1,30)
  describePortfolioBitRow(componentList,"contract/fleet",400,250,1,250)
  componentList.push(
    describeComponent('contract/'+marketOrder[0],605,400,'bit',1),
    describeComponent('contract/'+marketOrder[1],805,400,'bit',1),
    describeComponent('contract/'+marketOrder[2],1005,400,'bit',1),
    describeComponent('contract/'+marketOrder[3],1205,400,'bit',1),
    describeComponent('contract/'+marketOrder[4],1405,400,'bit',1),
    describeComponent('unit/merchant',830,-325,'bit',30),
    describeComponent('unit/white',830,-65,'bit',75),
    describeComponent('gold/1',1100,-325,'bit',50),
    describeComponent('gold/5',1100,-65,'bit',50),
    describeComponent('good/1',1370,-325,'bit',50),
    describeComponent('good/5',1370,-65,'bit',50)
  )
  console.log('componentList test:', componentList)
  loadComponents(componentList,message.updates);
}
