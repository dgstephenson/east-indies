Snap.plugin( function( Snap, Element, Paper, global ) {

  let rotating = false

  const mouseClick = event => {
    console.log("mouseClick")
  }

  const dragStart = function(x,y,event) {
    if(event.button===0) {
      this.data('ot', this.transform().local)
      this.data('dragging', true)
      this.data('rotating', false)
    }
  }

  const dragMove = function(dx, dy, event, x, y) {
    if(this.data('dragging')) {
      this.data('moved',true)
      if(rotating && false) {
        this.transform(this.data('ot') + "r" + dx)
      } else if(true) {
        const snapInvMatrix = this.transform().diffMatrix.invert()
        snapInvMatrix.e = 0
        snapInvMatrix.f = 0
        const tdx = snapInvMatrix.x( dx,dy )
        const tdy = snapInvMatrix.y( dx,dy )
        this.transform(`t${tdx},${tdy}${this.data('ot')}`)
      }
    } 
  }

  const dragEnd = function() {
    this.data('dragging', false)
  }

  this.onkeydown = event => {
    if(event.key==="Shift") rotating = true
  }

  this.onkeyup = event => {
    if(event.key==="Shift") rotating = false
  }

  Element.prototype.smartdrag = function() {
    this.drag(dragMove, dragStart, dragEnd)
    this.mousedown(mouseClick)
    return this
  }

})
