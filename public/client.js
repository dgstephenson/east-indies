/* global io */
const paper = Snap(window.innerWidth,window.innerHeight)
const tabletop = paper.group()

const socket = io({transports: ['websocket'], upgrade: false})
const templates = {} 
const boards = []
const bits = []

let templateFiles = []
let componentList = []
let panning = false
let seed = ""
let restartNeeded = false 

const unique = arr => {
  const u = {}
  return arr.filter(v => u[v] = (v !== undefined && !u.hasOwnProperty(v)) )
}

document.oncontextmenu = () => false

paperError = (error,paper) => console.log(error,paper)
paper.zpd({zoom:true,pan:false,drag:false},paperError)
paper.zoomTo(0.25,0)

paper.mousedown(event => { 
  if(event.button==2) paper.zpd({pan:true},paperError) 
})

paper.mouseup(event => {
  if(event.button==2) paper.zpd({pan:false},paperError)
})

const addFragment = (fragment,x,y,rotation) => {
  const svg = fragment.select("g")
  paper.append(svg)
  const children = paper.children()
  const component = children[children.length-1]
  const width = component.getBBox().width
  const height = component.getBBox().height
  const zoom = paper.zpd('save').a
  const startX = 0.5*(4000-width)+x
  const startY = 0.5*(2400-height)+y
  const startMatrix = component.transform().localMatrix.translate(startX,startY)
  startMatrix.rotate(rotation,width/2,height/2)
  component.transform(startMatrix)
  tabletop.add(component)
  return component
}

const setupTemplate = (file,componentList,updates) => fragment => {
  const template = addFragment(fragment,0,0,0)
  template.node.style.display = "none"
  templates[file] = template
  if(Object.keys(templates).length===templateFiles.length) {
    componentList.map(description=>addComponent(description))
    updates.map(processUpdate)
    setInterval(updateServer,400)
  }
}

const addComponent = (description) => {
  const x = description.x
  const y = description.y
  const rotation = description.rotation
  const type = description.type
  const clones = description.clones
  const file = description.file
  const template = templates[file]
  const startMatrix = template.transform().localMatrix.translate(x,y)
  for(i=0; i<clones; i++) {
    const component = template.clone()
    tabletop.add(component)
    component.node.style.display = "block"
    component.transform(startMatrix)
    component.transform(`${component.transform().local}r${rotation}`)
    if(type==="board") boards.push(component)
    else if(type==="bit") {
      bits.push(component)
      component.smartdrag()
      component.data('id',bits.length-1)
      component.data('file',file)
    }
  }
}

const loadComponents = (componentList,updates) => {
  templateFiles = unique(componentList.map(item=>item.file))
  templateFiles.map(file=>
    Snap.load(`assets/${file}.svg`,setupTemplate(file,componentList,updates))
  )
}

const describeComponent = (file,x,y,type="bit",clones=0,rotation=0) => (
  {file,x,y,type,clones,rotation}
)

const updateServer = () => {
  const msg = { updates: [] };
  bits.map(bit => {
    if(bit.data('moved')) {
      msg.updates.push({
        id: bit.data('id'),
        front: bit.data('front'),
        local: bit.transform().local
      })
      bit.data('moved',false)
    }
  })
  socket.emit('updateServer',msg)
}

const processUpdate = update => {
  const bit = bits[update.id]
  const local = bit.transform().local
  bit.stop()
  bit.animate({transform: update.local},400)
}

socket.on('connect', () => {

  socket.on('updateClient', msg => {
    if(msg.seed===seed) msg.updates.map(processUpdate)
  })

  socket.on('setup', msg => {
    if(seed==="") {
      seed = msg.seed
      Math.seedrandom(seed)
      setup(msg)
    } else {
      console.log("Restart Needed")
    }
  })

})
